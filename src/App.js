import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(){
  super()
  this.state={
    searchQuery:" "
  }
  this.handleChange=this.handleChange.bind(this)
}
handleChange(e){
  this.setState({
    searchQuery:e.target.value
  })
}

handleClick(){
  console.log('helo')
}
  
  render() {
    return (
      <div>
        <Search searchQuery={this.state.searchQuery} handleChange={this.handleChange} handleClick={this.handleClick}/>
        <Result/>
      </div>
    );
  } 
}

class Search extends React.Component{
  render(){
    console.log(this.props.searchQuery)
    return (
      <div>
        <input type="text" name="name" value={this.props.searchQuery} onChange={this.props.handleChange}/>
        <input type="button" name="button" value="search" onClick={this.props.handleClick}/>
       </div>
    )
  }
}

class Result extends React.Component{
  render(){
    return(
      <div>
        <ul>
          <li>welcome</li>
        </ul>
      </div>
    )
  }
}

export default App;



